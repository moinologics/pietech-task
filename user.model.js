const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	id: mongoose.Schema.Types.ObjectId,
	name: String,
	age: Number
});

module.exports = mongoose.model('User', userSchema);
