const express = require('express');
const mongoose = require('mongoose');
const User = require('./user.model');
require('dotenv').config();

const app = express();
const PORT = 8000;

mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true, useUnifiedTopology: true
}).catch(console.log);

app.get('/users', async (req, res) => {
  const { page = 1, limit = 10, searchByName = '', sortByAgeOrder = 1 } = req.query;
  const users = await User.find({name: {$regex: searchByName, $options: 'i'}})
                    .limit(limit)
                    .skip(limit * (page - 1))
                    .sort({age: sortByAgeOrder});
  const allUsersCount = await User.countDocuments();
  return res.json({
    users,
    total_pages: Math.ceil(allUsersCount / limit),
    current_page: page
  });
});


app.listen(PORT, () => console.log(`server listening on port ${PORT}`));
